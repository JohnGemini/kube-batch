/*
Copyright 2019 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package util

import (
	"fmt"
	"context"
	"math/rand"
	"sort"
	"sync"
	"strings"

	"github.com/golang/glog"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/errors"
	"k8s.io/client-go/util/workqueue"
	"k8s.io/kubernetes/pkg/scheduler/algorithm"
	schedulerapi "k8s.io/kubernetes/pkg/scheduler/api"
	"k8s.io/kubernetes/pkg/scheduler/cache"

	"github.com/kubernetes-sigs/kube-batch/pkg/scheduler/api"
	"github.com/kubernetes-sigs/kube-batch/pkg/scheduler/framework"
)

// HostPriority represents the priority of scheduling to a particular host, higher priority is better.
type HostPriority struct {
	// Name of the host
	Host string
	// Score associated with the host
	Score float64
}

// HostPriorityList declares a []HostPriority type.
type HostPriorityList []HostPriority

func (h HostPriorityList) Len() int {
	return len(h)
}

func (h HostPriorityList) Less(i, j int) bool {
	if h[i].Score == h[j].Score {
		return h[i].Host < h[j].Host
	}
	return h[i].Score < h[j].Score
}

func (h HostPriorityList) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
}

// PredicateNodes returns nodes that fit task
func PredicateNodes(task *api.TaskInfo, nodes []*api.NodeInfo, fn api.PredicateFn) []*api.NodeInfo {
	var predicateNodes []*api.NodeInfo

	var workerLock sync.Mutex
	checkNode := func(index int) {
		node := nodes[index]
		glog.V(3).Infof("Considering Task <%v/%v> on node <%v>: <%v> vs. <%v>",
			task.Namespace, task.Name, node.Name, task.Resreq, node.Idle)

		// TODO (k82cn): Enable eCache for performance improvement.
		if err := fn(task, node); err != nil {
			glog.Errorf("Predicates failed for task <%s/%s> on node <%s>: %v",
				task.Namespace, task.Name, node.Name, err)
			return
		}

		workerLock.Lock()
		predicateNodes = append(predicateNodes, node)
		workerLock.Unlock()
	}

	workqueue.ParallelizeUntil(context.TODO(), 16, len(nodes), checkNode)
	return predicateNodes
}

// PrioritizeNodes returns a map whose key is node's score and value are corresponding nodes
func PrioritizeNodes(
	task *api.TaskInfo,
	filterNodes []*api.NodeInfo,
	priorityConfigs []algorithm.PriorityConfig,
) (HostPriorityList, error) {
	nodeNameToInfo, nodes := generateNodeMapAndSlice(filterNodes)
	var (
		mu   = sync.Mutex{}
		wg   = sync.WaitGroup{}
		errs []error
	)
	appendError := func(err error) {
		mu.Lock()
		defer mu.Unlock()
		errs = append(errs, err)
	}

	results := make([]schedulerapi.HostPriorityList, len(priorityConfigs), len(priorityConfigs))

	for i, priorityConfig := range priorityConfigs {
		if priorityConfig.Function != nil {
			wg.Add(1)
			go func(index int, config algorithm.PriorityConfig) {
				defer wg.Done()
				var err error
				results[index], err = config.Function(task.Pod, nodeNameToInfo, nodes)
				if err != nil {
					appendError(err)
				}
			}(i, priorityConfig)
		} else {
			results[i] = make(schedulerapi.HostPriorityList, len(nodes))
		}
	}
	processNode := func(index int) {
		nodeInfo := nodeNameToInfo[nodes[index].Name]
		var err error
		for i := range priorityConfigs {
			if priorityConfigs[i].Function != nil {
				continue
			}
			results[i][index], err = priorityConfigs[i].Map(task.Pod, nil, nodeInfo)
			if err != nil {
				appendError(err)
				return
			}
		}
	}
	workqueue.ParallelizeUntil(context.TODO(), 16, len(nodes), processNode)
	for i, priorityConfig := range priorityConfigs {
		if priorityConfig.Reduce == nil {
			continue
		}
		wg.Add(1)
		go func(index int, config algorithm.PriorityConfig) {
			defer wg.Done()
			if err := config.Reduce(task.Pod, nil, nodeNameToInfo, results[index]); err != nil {
				appendError(err)
			}
			if glog.V(10) {
				for _, hostPriority := range results[index] {
					glog.Infof("%v/%v -> %v: %v, Score: (%d)", task.Pod.Namespace, task.Pod.Name, hostPriority.Host, config.Name, hostPriority.Score)
				}
			}
		}(i, priorityConfig)
	}
	// Wait for all computations to be finished.
	wg.Wait()
	if len(errs) != 0 {
		return HostPriorityList{}, errors.NewAggregate(errs)
	}

	// Summarize all scores.
	result := make(HostPriorityList, 0, len(nodes))
	for i := range nodes {
		result = append(result, HostPriority{Host: nodes[i].Name, Score: 0})
		for j := range priorityConfigs {
			result[i].Score += float64(results[j][i].Score * priorityConfigs[j].Weight)
		}
	}

	return result, nil
}

// SortNodes returns nodes by order of score
func SortNodes(priorityList HostPriorityList, nodesInfo map[string]*api.NodeInfo) []*api.NodeInfo {
	var nodesInorder []*api.NodeInfo

	sort.Sort(sort.Reverse(priorityList))

	for _, hostPriority := range priorityList {
		node := nodesInfo[hostPriority.Host]
		nodesInorder = append(nodesInorder, node)
	}

	return nodesInorder
}

// SelectBestNode returns best node whose score is highest, pick one randomly if there are many nodes with same score.
func SelectBestNode(priorityList HostPriorityList) string {
	maxScores := findMaxScores(priorityList)
	ix := rand.Intn(len(maxScores))
	return priorityList[maxScores[ix]].Host
}

// findMaxScores returns the indexes of nodes in the "priorityList" that has the highest "Score".
func findMaxScores(priorityList HostPriorityList) []int {
	maxScoreIndexes := make([]int, 0, len(priorityList)/2)
	maxScore := priorityList[0].Score
	for i, hp := range priorityList {
		if hp.Score > maxScore {
			maxScore = hp.Score
			maxScoreIndexes = maxScoreIndexes[:0]
			maxScoreIndexes = append(maxScoreIndexes, i)
		} else if hp.Score == maxScore {
			maxScoreIndexes = append(maxScoreIndexes, i)
		}
	}
	return maxScoreIndexes
}

// GetNodeList returns values of the map 'nodes'
func GetNodeList(nodes map[string]*api.NodeInfo) []*api.NodeInfo {
	result := make([]*api.NodeInfo, 0, len(nodes))
	for _, v := range nodes {
		result = append(result, v)
	}
	return result
}

func generateNodeMapAndSlice(nodes []*api.NodeInfo) (map[string]*cache.NodeInfo, []*v1.Node) {
	var nodeMap map[string]*cache.NodeInfo
	var nodeSlice []*v1.Node
	nodeMap = make(map[string]*cache.NodeInfo)
	for _, node := range nodes {
		nodeInfo := cache.NewNodeInfo(node.Pods()...)
		nodeInfo.SetNode(node.Node)
		nodeMap[node.Name] = nodeInfo
		nodeSlice = append(nodeSlice, node.Node)
	}
	return nodeMap, nodeSlice
}

// GetDependPodGroups returns a list of PodGroups on which the PodGroup pg depends
func GetDependPodGroups(ssn *framework.Session, pg *api.PodGroup) ([]*api.PodGroup, error) {
    var (
        self_step_name, job_uuid string
        depend_steps []string
        podGroups []*api.PodGroup
    )

    if depends_on, found := pg.Annotations[api.DependencyAnnotationKey]; found {
        if depends_on != "" {
            depend_steps = append(
                depend_steps,
                strings.Split(strings.ReplaceAll(depends_on, " ", ""), ",")...
            )
        } else {
            return podGroups, nil
        }
    } else {
        return nil, fmt.Errorf("There is no annotation <%v> in PodGroup %v/%v",
                               api.DependencyAnnotationKey, pg.Namespace, pg.Name)
    }

    if step, found := pg.Labels[api.StepLabelKey]; found {
        self_step_name = step
    } else {
        return nil, fmt.Errorf("There is no label <%v> in PodGroup %v/%v",
                               api.StepLabelKey, pg.Namespace, pg.Name)
    }

    if uuid, found := pg.Labels[api.JobUUIDLabelKey]; found {
        job_uuid = uuid
    } else {
        return nil, fmt.Errorf("There is no label <%v> in PodGroup %v/%v",
                               api.JobUUIDLabelKey, pg.Namespace, pg.Name)
    }

    labelSelector := metav1.LabelSelector{
        MatchExpressions: []metav1.LabelSelectorRequirement {
            metav1.LabelSelectorRequirement {
                Key:      api.StepLabelKey,
                Operator: metav1.LabelSelectorOpIn,
                Values:   depend_steps,
            },
        },
        MatchLabels: map[string]string {
            api.JobUUIDLabelKey: job_uuid,
        },
    }
    var selectorString string
    selector, err := metav1.LabelSelectorAsSelector(&labelSelector)
    if err != nil {
        return nil, fmt.Errorf("Can not get selector: %v", err)
    } else {
        selectorString = selector.String()
    }
    listOptions := metav1.ListOptions {
        LabelSelector: selectorString,
    }

    var steps map[string]bool
    if pg.Version == api.PodGroupVersionV1Alpha1 {
        pgs, err := ssn.GetClient().SchedulingV1alpha1().PodGroups(
            pg.Namespace).List(listOptions)
        if err != nil {
            return nil, fmt.Errorf("Can not get podGroups by selector <%v>: %v", selectorString, err)
        } else if pgInfo, steps_map, err := api.ConvertV1Alpha1ListToPodGroupInfoListAndSteps(pgs.Items, self_step_name); err != nil {
            return nil, fmt.Errorf("Can not get PodGroupInfo list and steps: %v", err)
        } else {
            podGroups = pgInfo
            steps = steps_map
        }
    } else if pg.Version == api.PodGroupVersionV1Alpha2 {
        pgs, err := ssn.GetClient().SchedulingV1alpha2().PodGroups(
            pg.Namespace).List(listOptions)
        if err != nil {
            return nil, fmt.Errorf("Can not get podGroups by selector <%v>: %v", selectorString, err)
        } else if pgInfo, steps_map, err := api.ConvertV1Alpha2ListToPodGroupInfoListAndSteps(pgs.Items, self_step_name); err != nil {
            return nil, fmt.Errorf("Can not get PodGroupInfo list and steps: %v", err)
        } else {
            podGroups = pgInfo
            steps = steps_map
        }
    }

    for _, depend_step := range depend_steps {
        if _, found := steps[depend_step]; !found {
            return nil, fmt.Errorf("There is no dependent step <%v> in the podGroups", depend_step)
        }
    }
    return podGroups, nil
}

// tasksFinished returns a result whether the number of finished tasks of a PodGroup is greater than or equal to its minMember
func tasksFinished(pg *api.PodGroup) bool {
    if pg.Status.Phase != api.PodGroupPending {
        return false
    }
    return pg.Spec.MinMember <= (pg.Status.Failed + pg.Status.Succeeded)
}

// DependencyValidation checks a PodGroup whether it meets the conditions of the given dependency policy
func DependencyValidation(pg *api.PodGroup, dependency_policy api.PodGroupDependencyPolicy) error {
    step_name := pg.Labels[api.StepLabelKey]
    if &pg.Status == nil {
        return fmt.Errorf("There is no status information of PodGroup %v/%v of step <%v>",
                           pg.Namespace, pg.Name, step_name)

    } else if pg.Spec.MinMember <= 0 {
        return fmt.Errorf("The minMember defined in PodGroup %v/%v of step <%v> must be greater than 0",
                          pg.Namespace, pg.Name, step_name)
    } else if !tasksFinished(pg) {
        return fmt.Errorf("The tasks in PodGroup %v/%v of step <%v> has not finished",
                          pg.Namespace, pg.Name, step_name)
    } else if dependency_policy == api.PodGroupAfterOK && pg.Status.Failed > 0 {
        return fmt.Errorf("There are failed tasks in PodGroup %v/%v of step <%v>",
                          pg.Namespace, pg.Name, step_name)
    } else if dependency_policy == api.PodGroupAfterNotOK && pg.Status.Failed == 0 {
        return fmt.Errorf("There are not failed tasks in PodGroup %v/%v of step <%v>",
                          pg.Namespace, pg.Name, step_name)
    }
    return nil
}
